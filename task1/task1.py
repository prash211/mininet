#!/usr/bin/python

'This excercise shows how to create a wireless network and how to create links between APs'

from mininet.node import Controller
from mininet.log import setLogLevel, info
from mn_wifi.cli import CLI_wifi
from mn_wifi.net import Mininet_wifi

def topology():
    "Create a network."
    net = Mininet_wifi(controller=Controller)

    info("*** Creating nodes\n") 
    sta1 = net.addStation('STA1', passwd='19025262', encrypt='wpa2', mac='00:00:00:10:11:08', ip='147.192.10.2/24', ip6='fe80::10', position='50,25,0', range=30, antennaGain =7, antennaHeight =3, min_v=5, max_v=5)
    sta2 = net.addStation('STA2', passwd='19025262', encrypt='wpa2', mac='00:00:00:10:11:09', ip='147.192.10.3/24', ip6='fe80::11', position='50,25,0', range=30, antennaGain =7, antennaHeight =3, min_v=5, max_v=10)
    sta3 = net.addStation('STA3', passwd='19025262', encrypt='wpa2', mac='00:00:00:10:11:10', ip='147.192.10.4/24', ip6='fe80::12', position='50,25,0', range=30, antennaGain =7, antennaHeight =3, min_v=5, max_v=7)
    ap1 = net.addAccessPoint('AP1', mac='00:00:00:00:10:02', ssid='ssid-ap1' , mode='a', channel='1', passwd='19025262', encrypt='wpa2', failmode='standalone', datapath='user', model='DI524', position='50,75,0', range=25)
    ap2 = net.addAccessPoint('AP2', mac='00:00:00:00:10:03', ssid='ssid-ap2' , mode='g', channel='6', passwd='19025262', encrypt='wpa2', failmode='standalone', datapath='user', model='DI524', position='50,125,0', range=25)
    ap3 = net.addAccessPoint('AP3', mac='00:00:00:00:10:04', ssid='ssid-ap3' , mode='b', channel='2', passwd='19025262', encrypt='wpa2', failmode='standalone', datapath='user', model='DI524', position='100,125,0', range=25)
    ap4 = net.addAccessPoint('AP4', mac='00:00:00:00:10:05', ssid='ssid-ap4' , mode='g', channel='3', passwd='19025262', encrypt='wpa2', failmode='standalone', datapath='user', model='DI524', position='150,125,0', range=25)
    ap5 = net.addAccessPoint('AP5', mac='00:00:00:00:10:06', ssid='ssid-ap5' , mode='b', channel='3', passwd='19025262', encrypt='wpa2', failmode='standalone', datapath='user', model='DI524', position='150,75,0', range=25)
    ap6 = net.addAccessPoint('AP6', mac='00:00:00:00:10:07', ssid='ssid-ap6' , mode='g', channel='3', passwd='19025262', encrypt='wpa2', failmode='standalone', datapath='user', model='DI524', position='200,75,0', range=25)
    UDPS = net.addStation('UDPS', passwd='19025262', encrypt='wpa2', mac='00:00:00:10:01:01', ip='147.192.10.1/24', position='50,140,0', range=30, antennaHeight=3, antennaGain=7)
    c0 = net.addController('c0')

    info("*** Configuring wifi nodes\n")
    net.configureWifiNodes()
    net.plotGraph(max_x=250, max_y=250)

    #Adding a config function

    info("*** Associating Stations\n")
    net.addLink(ap1,ap2, bw=800, delay='3ms', loss=5)
    net.addLink(ap2,ap3, bw=800, delay='3ms', loss=5) 
    net.addLink(ap3,ap4, bw=800, delay='3ms', loss=5) 
    net.addLink(ap3,ap5, bw=800, delay='3ms', loss=5) 
    net.addLink(ap5,ap6, bw=800, delay='3ms', loss=5) 

    #Adding mobility
    net.startMobility(time=0)
    net.mobility(sta1, 'start', time=10, position='50,25,0' )
    net.mobility(sta1, 'stop', time=20, position='200,75,0' )
    net.mobility(sta2, 'start', time=15, position='50,25,0' )
    net.mobility(sta2, 'stop', time=21, position='150,125,0')
    net.mobility(sta3, 'start', time=16, position='50,25,0' )
    net.mobility(sta3, 'stop', time=22, position='100,125,0')
    net.stopMobility(time=30)



    info("*** Starting network\n")
    net.build()
    c0.start()
    ap1.start([c0])
    ap2.start([c0])
    ap3.start([c0])
    ap4.start([c0])
    ap5.start([c0])
    ap6.start([c0])

    info("*** Running CLI\n")
    CLI_wifi(net)

    info("*** Stopping network\n")
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    topology()
