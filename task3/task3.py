from mininet.topo import Topo  
from mininet.node import OVSSwitch
from mininet.link import TCLink




class MyTopo( Topo ):  
    def __init__( self ):
	# Initialize topology
    	Topo.__init__( self )

        # Add hosts and switches
	h1 = self.addHost('H1', mac='00:00:00:00:00:01', ip='147.197.129.1/24')
	h2 = self.addHost('H2', mac='00:00:00:00:00:02', ip='147.197.129.2/24')
	h3 = self.addHost('H3', mac='00:00:00:00:00:03', ip='147.197.129.3/24')
	h4 = self.addHost('H4', mac='00:00:00:00:00:04', ip='147.197.129.4/24')
	h5 = self.addHost('H5', mac='00:00:00:00:00:05', ip='147.197.129.5/24')
	h6 = self.addHost('H6', mac='00:00:00:00:00:06', ip='147.197.129.6/24')
	h7 = self.addHost('H7', mac='00:00:00:00:00:07', ip='147.197.129.7/24')
	h8 = self.addHost('H8', mac='00:00:00:00:00:08', ip='147.197.129.8/24')
	h9 = self.addHost('H9', mac='00:00:00:00:00:09', ip='147.197.129.9/24')
	h10 = self.addHost('H10', mac='00:00:00:10:10:01', ip='147.197.129.10/24')
	h11 = self.addHost('H11', mac='00:00:00:10:10:02', ip='147.197.129.11/24')
	h12 = self.addHost('H12', mac='00:00:00:10:10:03', ip='147.197.129.12/24')
	Server = self.addHost('Server', mac='00:00:00:10:10:04', ip='10.10.10.1/8')

	Switch1 = self.addSwitch('S1', cls=OVSSwitch)
	Switch2 = self.addSwitch('S2', cls=OVSSwitch)
	Switch3 = self.addSwitch('S3', cls=OVSSwitch)
	Switch4 = self.addSwitch('S4', cls=OVSSwitch)
	Switch5 = self.addSwitch('S5', cls=OVSSwitch)
	Switch6 = self.addSwitch('S6', cls=OVSSwitch)
	Switch7 = self.addSwitch('S7', cls=OVSSwitch)
	Switch8 = self.addSwitch('S8', cls=OVSSwitch)
			
	# Add links between switches and hosts
	self.addLink(h1, Switch5, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h2, Switch5, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h3, Switch5, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h4, Switch6, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h5, Switch6, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h6, Switch6, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h7, Switch7, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h8, Switch7, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h9, Switch7, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h10, Switch8, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h11, Switch8, cls=TCLink, bw=100, delay='0ms', loss=0 )
	self.addLink(h12, Switch8, cls=TCLink, bw=100, delay='0ms', loss=0 )

	#Links between switches
	self.addLink(Switch1, Switch2, cls=TCLink, bw=500, delay='1ms', loss=0.5)
	self.addLink(Switch1, Switch3, cls=TCLink, bw=500, delay='1ms', loss=0.5)
	self.addLink(Switch1, Switch4, cls=TCLink, bw=500, delay='1ms', loss=0.5)
	self.addLink(Switch2, Switch4, cls=TCLink, bw=500, delay='1ms', loss=0.5)
	self.addLink(Switch2, Switch3, cls=TCLink, bw=500, delay='1ms', loss=0.5)
	self.addLink(Switch3, Switch4, cls=TCLink, bw=500, delay='1ms', loss=0.5)
	self.addLink(Switch3, Switch5, cls=TCLink, bw=500, delay='1ms', loss=0.5)
	self.addLink(Switch3, Switch6, cls=TCLink, bw=500, delay='1ms', loss=0.5)
	self.addLink(Switch4, Switch7, cls=TCLink, bw=500, delay='1ms', loss=0.5)
	self.addLink(Switch4, Switch8, cls=TCLink, bw=500, delay='1ms', loss=0.5)

	#link between Server and switch2
	self.addLink(Server, Switch2, cls=TCLink, bw=1000, delay='0ms', loss=0)
        
topos = { 'mytopo': ( lambda: MyTopo() ) }


