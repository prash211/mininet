#!/usr/bin/python
import sys
from mininet.log import setLogLevel, info
from mn_wifi.cli import CLI_wifi
from mn_wifi.net import Mininet_wifi
from mn_wifi.link import wmediumd,adhoc
from mn_wifi.wmediumdConnector import interference

def topology():
    "Create a network."
    net = Mininet_wifi(link=wmediumd, wmediumd_mode= interference)

    info("*** Creating nodes\n")
    sta4adhoc = net.addStation('sta4adhoc', mac='00:00:00:00:00:05', ip6='fe80::13', position='125,150,0', range=40, antennaGain =5, antennaHeight =1, min_v=5, max_v=5  )
    sta5adhoc = net.addStation('sta5adhoc', mac='00:00:00:00:00:06', ip6='fe80::14', position='150,150,0', range=40, antennaGain =6, antennaHeight =2, min_v=5, max_v=10 )
    sta6adhoc = net.addStation('sta6adhoc', mac='00:00:00:00:00:07', ip6='fe80::15', position='175,150,0', range=40, antennaGain =7, antennaHeight =3, min_v=2, max_v=7  )

    info("*** Configuring wifi nodes\n")
    net.configureWifiNodes()
    net.plotGraph(max_x=300, max_y=300)

    info("*** Associating Stations\n")
    net.addLink(sta4adhoc, cls=adhoc, ssid='adhocUH', mode='g',  proto='babel', channel=5, ht_cap='HT40+')
    net.addLink(sta5adhoc, cls=adhoc, ssid='adhocUH', mode='g',  proto='babel', channel=5, ht_cap='HT40+')
    net.addLink(sta6adhoc, cls=adhoc, ssid='adhocUH', mode='g',  proto='babel', channel=5, ht_cap='HT40+')

    #Adding mobility
    net.startMobility(time=15)
    net.mobility(sta4adhoc, 'start', time=19, position='125,150,0')
    net.mobility(sta4adhoc, 'stop',  time=20, position='200,250,0')
    net.mobility(sta5adhoc, 'start', time=18, position='150,150,0')
    net.mobility(sta5adhoc, 'stop',  time=21, position='225,250,0')
    net.mobility(sta6adhoc, 'start', time=17, position='175,150,0')
    net.mobility(sta6adhoc, 'stop',  time=22, position='250,250,0')
    net.stopMobility(time=25)


    info("*** Starting network\n")
    net.build()
    
    info("*** Running CLI\n")
    CLI_wifi(net)

    info("*** Stopping network\n")
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    topology()
